#!/bin/bash

setup_pi_zwave() {

sudo echo "dtoverlay=disable-bt" >> /boot/config.txt
sudo raspi-config nonint do_serial 1
sudo sed -i 's/enable_uart=0/enable_uart=1/g' /boot/config.txt
sudo apt-get update
sudo apt-get install -y python-pip git python3-gpiozero
python -m pip install pyserial
echo '*------'
echo "*------ Restart required ------*"
echo '------*'
}

setup_pi_zwave