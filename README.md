# Kohost Zwave NVM Tester for Raspberry Pi 4

This is a setup and shell utility that will configure a Raspberry Pi 4 for communication with a 500 series Zwave chip.

# Prerequesites

- A connected 500 series chip via UART to the Raspberry Pi 4
- The chip must be programmed with the SerialAPI
- User must have sudo access

# Usage

Clone this repository.

`git clone git@bitbucket.org:kohost/utils-iot-test.git kohost-iot-test`

Run the setup script

`cd kohost-iot-test && sudo sh setup.sh`

The script will configure the Raspberry Pi to accept serial communication to/from the 500 series ship. It will then install all dependecies required for the NVM utility to run.

If the script runs successfully, you'll be prompted to reboot.

`sudo reboot`

## Testing Zwave

Navigate back to the `kohost-iot-test` directory, inside will be a `zwave` folder.

`cd zwave`

Execute the test CLI program.

`sudo python test.py`

If setup correctly, on script start, you should see information about the chip. Example output:

```
COM Port set to /dev/ttyAMA0
SerialAPI Ver=5.4
Mfg=0147
ProdID/TypeID=400:02
Z-Wave 4.05 SDK 6.51.06 06/2015 or SDK 6.51.05 12/2014
Library=1 Static Controller
NodeIDs= 1,
```

If errors are output, the 500 series chip may not have the SerialAPI installed properly.

Type `?` into the CLI to get a list of options available.

Type `x` to exit the program.

## Testing Infrared

Navigate back to the `kohost-iot-test` directory, inside will be a, `ir` folder.

`cd zwave`

Execute the test program to bring GPIO Pin 3 on for 1 second, then off for 1 second. This will repeat infinitely until the program is exited.

**Note that python3 is used here.**

`sudo python3 test.py`

# Credits

- DrZwave for the Python utility: https://github.com/drzwave/TestNVM.git
- GPIO Zero for IR Test Utility
